﻿function Add-TTF() {
param(
    [switch] $Help,
    [string] $Font = "",
    [string] $Name = ""
)
# Variables
$Key = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont"
$FontPath = Join-Path -Path "$env:windir" -ChildPath "Fonts"
$HelpInfo = @'

    Function : Add-TTF
    By       : Derreck Kasper @ One101 Developers
    Based On : http://blogs.msdn.com/b/powershell/archive/2006/10/16/windows-powershell-font-customization.aspx
    Date     : 08/21/2012
    Purpose  : Installs TrueType or OpenType fonts into cmd.exe or powershell.exe console
    Usage    : Add-TTF -font font.ttf -name font name
    Note     : Requires run as Administrator

'@
if ($Help)
    {
    Write-Host $HelpInfo
    Return
    }
# Start conditional statements
if ($Font -eq "")
    {
    Write-Host $HelpInfo
    Return
    }
if (!(Test-Path "$FontPath\$Font"))
    {
        Write-Host "ERROR: Font $Font not installed on computer."
        Break
    }
# End conditional statements
$ConsoleFont = Get-ItemProperty $Key |
                Get-Member |
                Where-Object { $_.Name -match "^0+$" } |
                Where-Object { $_.Definition -match "$Name" }
# Start conditional statement
if ($ConsoleFont -ne $null)
    {
        Write-Host "The font $Name is already installed as a cmd.exe / PS font."
        break
    }
# End conditional statement
# Check for existing count of zeros in registry key
$RegZeros = Get-ItemProperty $Key |
             Get-Member |
             Where-Object { $_.Name -match "^0+$" } |
             Measure-Object
$Count = $RegZeros.Count
# Install font into registry
New-ItemProperty $Key -Name ("0" * ($Count + 1)) -PropertyType String -Value "$Name"
Write-Host "The font $Name was successfully installed as a cmd.exe / PS font."
}
# If you don't want to add this to your profile and just run this as a script, uncomment below:
# . Add-TTF