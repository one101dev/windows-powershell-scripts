# Copyright 2012 Derreck Kasper

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

    # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# download_plugins.ps1
# Description:
#	This PowerShell script will download plugins for Internet Explorer and Firefox / Chrome (or any other plugin based browser).
#	Use this script to keep your plugins up to date.

Write-Host "-- One101 Developers --" -Back black -Fore blue
Write-Host "Download Browser Plugins v0.1" -Back black -Fore yellow
Write-Host "Created by Derreck Kasper" -Back black -Fore yellow

# The Select-String cmdlet is similar to grep in Linux. I select all the strings from the file download_list using a
# Regular Expression pattern match. In the example below, this selects all URIs that do not have a '# ' in front of them.
# The parenthesis indicate grouping of patterns, and for whatever reason, the second pattern does not want to allow usage
# of the '.' expression, which selects all text except periods and some other special characters. I was forced to work around
# this issue by using the character class [a-zA-Z0-9...]. Maybe it's a quirk with PowerShell?

# Debugging the regular expression match.
# Select-String -Path download_list -Pattern "(^[^\#\s.]*)((https?|ftp):[/]{2}[a-zA-Z0-9\./\-\&_]*)" -allmatches | Select -Expand Matches
$a = Select-String -Path download_list -Pattern "(^[^\#\s.]*)((https?|ftp):[/]{2}[a-zA-Z0-9\./\-\&_]*)" | Select -Expand Matches
$b = $a.value
$c = $b.length
$d1 = Get-Location
$d2 = $d1.Path
$e1 = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1"
# Write-Host $d2
Write-Host "Downloading a total of $c plugins."
Write-Host "Please wait..." -Back blue -Fore yellow
for($f1 = 1; $f1 -le $c; $f1++)
	{
		$f2 = $b[$f1]
		Write-Host $f2
		$e2 = New-Object System.Net.WebClient
#		$e2.Headers.Add("user-agent", $e1)
		$e2.DownloadFile($f2, $d2)
	}
# The rest of the script is not written yet!